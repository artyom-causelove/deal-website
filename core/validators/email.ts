import email from 'validator/lib/isEmail';

export function isEmail (
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  return (v: string) => {
    return email(v) || (error || `${fieldName || 'The field'} must be an email`);
  };
}
