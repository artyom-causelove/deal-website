import { union } from './union';

export class ContainsOptions {
  containsRules: {
    [symbolName: string]: (v: string) => boolean | string
  } = {};

  constructor(
    containsRules: { [symbolName: string]: (v: string) => boolean | string }
  ) {
    this.containsRules = containsRules;
  }

  get error (): string {
    let resError = 'The field must contains: ';

    for (const prop in this.containsRules) {
      resError += `${prop}, `;
    }

    return resError.slice(0, resError.length - 2);
  }
}

export function contains (
  symbols: ContainsOptions,
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  let resError = error || symbols.error;

  resError = fieldName ? fieldName + resError.slice(9) : resError;
  
  return union(Object.values(symbols), resError);
}
