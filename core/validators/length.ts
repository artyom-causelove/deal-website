export function maxLength (
  length: number,
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  return (v: string) => {
    return v.length <= length || (error || `${fieldName || 'The field'} must be less than ${length} symbols`);
  };
}

export function minLength (
  length: number,
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  return (v: string) => {
    return v.length >= length || (error || `${fieldName || 'The field'} must be longer than ${length} symbols`);
  };
}

export function length (
  length: number,
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  return (v: string) => {
    return v.length === length || (error || `${fieldName || 'The field'} must be ${length} symbols`);
  };
}
