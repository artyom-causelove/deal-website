export function union (
  fns: Array<(v: string) => boolean>,
  error: string
): (v: string) => boolean | string {
  return (v: string) => fns.every(fn => fn(v)) || error;
};
