import { ContainsOptions } from './contains';

export function containsOnly (
  symbols: ContainsOptions,
  fieldName?: string,
  error?: string
): (v: string) => boolean | string {
  let resError;

  if (error) {
    resError = error;
  } else {
    resError = `${fieldName || 'The field'} must contains:`
    for (const prop in symbols) {
      resError += `${prop}, `;
    }
    resError = resError.slice(0, resError.length - 2);
  }
}
